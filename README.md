# Arch to Parabola migrate script

This script automates the process of migrating from Arch to Parabola. 

Parabola install tends to be broken, and Parabola decided not to support archinstall. 
Script follows steps from https://wiki.parabola.nu/Migration_from_Arch

Currently GRUB / systemd only.

1) Install Arch

2) Download script

`wget https://gitlab.com/hacklab01/arch-to-parabola-migrate-script/-/raw/main/arch2parabola.sh`

3) Run script

`bash arch2parabola.sh`

