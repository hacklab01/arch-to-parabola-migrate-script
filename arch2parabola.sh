#/bin/bash
# Script follows steps from https://wiki.parabola.nu/Migration_from_Arch

# Ensure that your current system is fully up-to-date.

sudo pacman -Syu

# Install Parabola keyrings and mirror list
# Disable signature verification manually by modifying the line in /etc/pacman.conf:
# SigLevel    = Required DatabaseOptional
# SigLevel = Never

sudo sed -i 's/SigLevel\ \ \ \ = Required DatabaseOptional/SigLevel = Never/g' /etc/pacman.conf

# Install the keyrings and mirror list for free repositories

sudo pacman -U https://www.parabola.nu/packages/libre/x86_64/parabola-keyring/download
sudo pacman -U https://www.parabola.nu/packages/libre/x86_64/pacman-mirrorlist/download

# Re-enable signature verification in /etc/pacman.conf:
# SigLevel = Required DatabaseOptional

sudo sed -i 's/SigLevel = Never/SigLevel = Required DatabaseOptional/g' /etc/pacman.conf

# Rename mirrorlist.pacnew as mirrorlist:

sudo cp -vr /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist

# Except for special cases, the default Parabola pacman.conf may be used as a drop-in replacement, at this stage of the migration

wget https://git.parabola.nu/abslibre.git/plain/libre/pacman/pacman.conf.$(uname -m)
sudo cp /etc/pacman.conf /etc/pacman.conf.orig
sudo mv pacman.conf.$(uname -m) /etc/pacman.conf

# Replace non-free packages with Parabola liberated versions

# Clean the pacman cache:

sudo pacman -Scc

# Force the database refresh:

sudo pacman -Syy

# Update the keyring:

sudo pacman-key --refresh

# Sync the system with Parabola and install your-freedom. This will remove non-free packages and install libre replacements.

# For a parabola/systemd system:

sudo pacman -Suu your-freedom pacman

# For a parabola/openrc system:
# pacman -Suu your-freedom pacman libelogind udev-init-scripts

#Install the optional your-privacy package to block privacy disrespecting network protocols (see Nonprism):
#pacman -Suu your-privacy

# For grub regenerate your grub.cfg file running:

sudo grub-mkconfig -o /boot/grub/grub.cfg

# Done, reboot!

sudo reboot
